from setuptools import setup, find_packages

__version__ = "0.1.2"

with open("requirements.txt") as f:
    install_requirements = f.read().splitlines()

setup(
    name="ximilar-recognition-core",
    version=__version__,
    description="The Ximilar  Machine Learning Framework.",
    url="http://ximilar.com/",
    author="Michal Lukac, David Novak",
    author_email="michallukac@outlook.com",
    license="MIT License",
    packages=find_packages(),
    keywords="api, multimedia, json, rest, data",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 2.7",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Private :: Do Not Upload",
    ],
    install_requires=install_requirements,
    include_package_data=True,
    zip_safe=False,
    namespace_packages=["ximilar"],
)
