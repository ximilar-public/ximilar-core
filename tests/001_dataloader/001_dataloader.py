import base64
import cv2
import numpy as np
import unittest

from ximilar.api.common_api import Record
from ximilar.base.img_data_loader import ImgDataLoader
from ximilar.base.config import ArgumentParser
from ximilar.base.logger import Logger

IMGJPG1 = "../../ximilar/examples/data/cross.jpg"
IMGJPG2 = "../../ximilar/examples/data/mini2.jpg"
IMGPNG = "../../ximilar/examples/data/mushroom.png"


def load_image(path, color_space, shape=(224, 224)):
    image = cv2.imread(path)
    if color_space:
        image = cv2.cvtColor(image, color_space)

    image = cv2.resize(image, shape, interpolation=cv2.INTER_AREA).astype(np.uint8)
    return image


def load_base64_image(path):
    with open(path, "rb") as file_img:
        image = base64.b64encode(file_img.read()).decode("utf-8")
    return image


class TestDataLoader(unittest.TestCase):
    """
    Test suite for testing DataLoader/DataProcessor class for loading images.
    """

    @classmethod
    def setUpClass(self):
        """ get_some_resource() is slow, to avoid calling it for each test use setUpClass()
            and store the result as class variable
        """
        super(TestDataLoader, self).setUpClass()

        parser = ArgumentParser(
            description="A ImageDataLoader parser", parents=[ImgDataLoader.get_arg_parser(), Logger.get_arg_parser()]
        )
        args = parser.parse_args("rgb_config.yaml")

        self.data_loader = ImgDataLoader(args)

    def test_rec_file_url_data(self):
        original = load_image(IMGJPG1, cv2.COLOR_BGR2RGB)
        record = [{Record.FILE: IMGJPG1}, {Record.URL: "http://www.ximilar.com/examples/mountains_cross.jpg"}]

        output = self.data_loader.process_records(record)
        self.assertTrue(np.allclose(np.asarray(output[0][Record.IMG_DATA]), original))
        self.assertTrue(np.allclose(np.asarray(output[1][Record.IMG_DATA]), original, atol=100))
        self.assertTrue(np.allclose(np.asarray(output[1][Record.IMG_DATA]), output[0][Record.IMG_DATA], atol=100))

    def test_base64_file(self):
        original = load_image(IMGJPG1, cv2.COLOR_BGR2RGB)
        image_base64 = load_base64_image(IMGJPG1)
        record = [{Record.FILE: IMGJPG1}, {Record.BASE64: image_base64}]

        output = self.data_loader.process_records(record)
        self.assertTrue((np.asarray(output[0][Record.IMG_DATA]) == original.tolist()).all())
        self.assertTrue((np.asarray(output[1][Record.IMG_DATA]) == original.tolist()).all())
        self.assertTrue((np.asarray(output[1][Record.IMG_DATA]) == output[0][Record.IMG_DATA]).all())

    def test_base64_file2(self):
        original = load_image(IMGJPG2, cv2.COLOR_BGR2RGB)
        image_base64 = load_base64_image(IMGJPG1)
        record = [{Record.FILE: IMGJPG2}, {Record.BASE64: image_base64}]

        output = self.data_loader.process_records(record)

        self.assertFalse((np.asarray(output[1][Record.IMG_DATA]) == original.tolist()).all())
        self.assertTrue(np.allclose(np.asarray(output[0][Record.IMG_DATA]), original, atol=100))
        self.assertFalse((np.asarray(output[1][Record.IMG_DATA]) == output[0][Record.IMG_DATA]).all())

    def test_png_file(self):
        original = load_image(IMGPNG, cv2.COLOR_BGR2RGB)
        record = [{Record.FILE: IMGPNG}]

        output = self.data_loader.process_records(record)
        self.assertTrue((np.asarray(output[0][Record.IMG_DATA]) == original.tolist()).all())

    def test_color_space(self):
        hsv_orig = load_image(IMGJPG1, color_space=cv2.COLOR_BGR2HSV, shape=(100, 100))
        luv_orig = load_image(IMGJPG1, color_space=cv2.COLOR_BGR2LUV, shape=(100, 100))
        bgr_orig = load_image(IMGJPG1, color_space=None, shape=(100, 100))

        record = [{Record.FILE: IMGJPG1}]

        parser = ArgumentParser(description="A ImageDataLoader parser", parents=[ImgDataLoader.get_arg_parser()])
        args = parser.parse_args("hsv_config.yaml")
        hsv_data_loader = ImgDataLoader(args)
        output = hsv_data_loader.process_records(record)
        self.assertTrue(np.allclose(np.asarray(output[0][Record.IMG_DATA]), hsv_orig))

        parser = ArgumentParser(description="A ImageDataLoader parser", parents=[ImgDataLoader.get_arg_parser()])
        args = parser.parse_args("luv_config.yaml")
        luv_data_loader = ImgDataLoader(args)
        output = luv_data_loader.process_records(record)
        self.assertTrue(np.allclose(np.asarray(output[0][Record.IMG_DATA]), luv_orig, atol=100))

        parser = ArgumentParser(description="A ImageDataLoader parser", parents=[ImgDataLoader.get_arg_parser()])
        args = parser.parse_args("bgr_config.yaml")
        bgr_data_loader = ImgDataLoader(args)
        output = bgr_data_loader.process_records(record)
        self.assertTrue(np.allclose(np.asarray(output[0][Record.IMG_DATA]), bgr_orig, atol=100))


if __name__ == "__main__":
    unittest.main()
