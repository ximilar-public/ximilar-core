clean:
	rm -r ./build
	rm -r ./dist
	rm -r ./ximilar.egg-info
	rm -r ./wheelhouse

install:
	sudo pip install -r requirements.txt -f vendor/

build:
	python setup.py sdist bdist_wheel

create-req:
	pip-compile requirements.in -o requirements.txt -f vendor/
