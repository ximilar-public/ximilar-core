import json
import traceback
import copy
import time
import uuid
from flask import request, Response

from ximilar.api.common_api import Request
from ximilar.api.python_api import Record
from ximilar.api.common_api import Response as ApiResponse
from ximilar.api.processor import RequestProcessor
from ximilar.base.logger import Logger
from ximilar.base.exception import DeepException
from ximilar.base.config import ArgumentParser


class RESTProcessorView(object):
    """
    This class represents entry point for handling request for our flask server.

    It contains processor which implements method process_record for further
    processing of json_record/request.
    """

    @staticmethod
    def get_arg_parser(name="RESTProcessorView"):
        parser = ArgumentParser(name)
        parser.add_argument("--time_stats", help="If we want to compute the time stats", type=bool, default=True)
        return parser

    def __init__(self, args, processors, name="RESTProcessorView"):
        """
        Initialize object with processors. Processors can be data loaders, data augmenters,
        celery processor and so on. Each processor inherits from DataProcessor class.

        :param processors: [DataProcessor, DataProcessor...]
        :return: None
        """
        self.time_stats = args[name].time_stats
        self.id = str(uuid.uuid4())
        self.logger = Logger(args, name)

        # check that all variables are of the right type
        for m in processors:
            if not isinstance(m, RequestProcessor):
                raise Exception("The passed instance is not of type DataProcessor: " + str(m))

        self.processors = processors

        # all of the floats returned from service should be with 3 decimals
        from json import encoder

        encoder.FLOAT_REPR = lambda o: format(o, ".3f")

    def parse_request(self, request):
        """
        Returns tuple of (json data, headers) from request.
        """
        try:
            if request.json is None:
                raise DeepException(
                    code=ApiResponse.STATUS_CODE_BAD_REQUEST,
                    msg="Error during parsing request. No json data specified.",
                )

            if self.logger.log_debug:
                self.logger.debug(
                    "Processing request:" + json.dumps(self.logger.clean_json(copy.deepcopy(request.json)))
                )

            return request.json, request.headers
        except Exception:
            raise DeepException(code=ApiResponse.STATUS_CODE_BAD_REQUEST, msg="Error during parsing request.")

    def call_request_processors(self, request_data, request_headers):
        """
        Calls every request processor on request and modifies it if the condition of processor is satisfied.
        """
        for p in self.processors:
            self.logger.debug("Processing with " + str(p))

            try:
                if not p.pre_condition(request_data):
                    raise p.pre_condition_exception()

                self.logger.debug("Precondition of " + str(p) + " is successful: " + p.pre_condition_desc())
                request_data, request_headers = p.process_request(request_data, headers=request_headers)
                self.logger.debug("Processed request " + str(p))
            except:
                p.clean_response(request_data)
                raise

        return request_data

    def get_request_status(self, request_data):
        """
        Extract status dictionary from records or entire request.
        """
        if ApiResponse.STATUS not in request_data and Request.RECORDS in request_data:
            status_dict = self.check_records_status(request_data)
        elif ApiResponse.STATUS in request_data and isinstance(request_data[ApiResponse.STATUS], dict):
            status_dict = request_data[ApiResponse.STATUS]
        else:
            status_dict = {
                ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_ERROR,
                ApiResponse.STATUS_TEXT: "ERROR",
                ApiResponse.STATUS_ERROR: request_data["message"] if "message" in request_data else "Unexpected errror",
            }

        if ApiResponse.STATUS_CODE not in status_dict:
            status_dict[ApiResponse.STATUS_CODE] = ApiResponse.STATUS_CODE_OK

        return status_dict

    def process_record(self):
        """
        Process record by calling all possible processors and return http response.
        :return: HTTP Flask Response
        """
        request_data = None
        start_time = time.time()

        try:
            self.logger.debug("Processing by RESTProcessorView")

            request_data, request_headers = self.parse_request(request)
            request_data = self.call_request_processors(request_data, request_headers)
            request_data[ApiResponse.STATUS] = self.get_request_status(request_data)
            request_data = self.compute_time_stats(request_data, start_time)

            return Response(
                json.dumps(request_data),
                mimetype="application/json",
                status=request_data[ApiResponse.STATUS][ApiResponse.STATUS_CODE],
            )
        except DeepException as e:
            # Custom Exception Handling with status code
            self.logger.sentry_exception()
            request_data = self.compute_time_stats(request_data, start_time)
            return self.handle_exception(request_data, str(e), e.code)
        except Exception as e:
            self.logger.sentry_exception()
            self.logger.debug("Exception: " + str(e) + " StackTrace: " + traceback.format_exc())
            request_data = self.compute_time_stats(request_data, start_time)
            return self.handle_exception(request_data, str(e), ApiResponse.STATUS_CODE_ERROR)

    def check_records_status(self, json_record):
        """
        Checks every record and if everything is OK then status code is OK result,
                                if some of the results are bad then MIXED result,
                                if every record has error then ERROR result.
        """
        k = 0
        for i in range(len(json_record[Request.RECORDS])):
            if (
                Record.REC_STATUS in json_record[Request.RECORDS][i]
                and json_record[Request.RECORDS][i][Record.REC_STATUS][ApiResponse.STATUS_CODE]
                != ApiResponse.STATUS_CODE_OK
            ):
                k += 1

        if k != 0:
            try:
                self.logger.error(
                    "Error processing request: " + json.dumps(self.logger.clean_json(copy.deepcopy(json_record)))
                )
            except Exception:
                self.logger.sentry_exception()
                pass

        if k == len(json_record[Request.RECORDS]):
            return {ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_ERROR, ApiResponse.STATUS_TEXT: "ERROR"}

        if k > 0:
            return {
                ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_MIXED_RESULT,
                ApiResponse.STATUS_TEXT: "MIXED_RESULT",
            }

        return {ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_OK, ApiResponse.STATUS_TEXT: "OK"}

    def handle_exception(self, request_data, error_txt, status_code):
        """
        Returns HTTP Response of error when exception happens during processing of request.
        """
        if request_data is None or not isinstance(request_data, dict):
            request_data = {
                ApiResponse.STATUS: {ApiResponse.STATUS_CODE: status_code, ApiResponse.STATUS_TEXT: error_txt}
            }
        else:
            request_data[ApiResponse.STATUS] = {
                ApiResponse.STATUS_CODE: status_code,
                ApiResponse.STATUS_TEXT: error_txt,
            }

        try:
            json_response = json.dumps(request_data, ensure_ascii=False)
            return Response(json_response, mimetype="application/json", status=status_code)
        except Exception as e:
            self.logger.sentry_exception()
            self.logger.error("Error processing request")
            return Response(
                json.dumps(
                    {ApiResponse.STATUS: {ApiResponse.STATUS_CODE: status_code, ApiResponse.STATUS_TEXT: error_txt}}
                )
            )

    def compute_time_stats(self, result, start_time):
        """
        Computes time statistics of entire request processing.
        """
        if not self.time_stats or result is None or not isinstance(result, dict):
            result = {}

        total_time = time.time() - start_time
        # this can be already set from some request processor
        if ApiResponse.STATISTICS not in result:
            result[ApiResponse.STATISTICS] = {
                ApiResponse.STATS_PROC_TIME: total_time,
                ApiResponse.STATS_PROC_ID: self.id,
            }
        else:
            # some time stats already there but we need to update total time
            result[ApiResponse.STATISTICS][ApiResponse.STATS_PROC_TIME] = total_time

        # if we have statistic from each processor then store it into statistics
        if (
            Request.RECORDS in result
            and len(result[Request.RECORDS]) > 0
            and Record.TIME_STATS in result[Request.RECORDS][0]
        ):
            if Record.TIME_STATS not in result[ApiResponse.STATISTICS]:
                result[ApiResponse.STATISTICS][Record.TIME_STATS] = result[Request.RECORDS][0][Record.TIME_STATS]
            else:
                result[ApiResponse.STATISTICS][Record.TIME_STATS].update(result[Request.RECORDS][0][Record.TIME_STATS])

            # safely delete it from each record
            for i in range(len(result[Request.RECORDS])):
                if Record.TIME_STATS in result[Request.RECORDS][i]:
                    del result[Request.RECORDS][i][Record.TIME_STATS]

        if (
            not self.logger.log_debug
            and ApiResponse.STATISTICS in result
            and Record.TIME_STATS in result[ApiResponse.STATISTICS]
        ):
            del result[ApiResponse.STATISTICS][Record.TIME_STATS]

        return result
