import json
import signal
import traceback

from flask import request, Response

from ximilar.api import api_requests
from ximilar.base.config import ArgumentParser
from ximilar.base.logger import Logger
from ximilar.api import python_api
from ximilar.api.common_api import Response as ApiResponse

codes_dictionary = {
    api_requests.Status.SUCCESS: 200,
    api_requests.Status.PARTIAL: 200,
    api_requests.Status.ERROR: 500,
    api_requests.Status.WRONG_REQUEST: 400,
}


class RESTServer(object):
    """
    This class encapsulates the Flask-based REST server.
    """

    @staticmethod
    def get_arg_parser(name="RESTServer"):
        default_port = 4000
        default_address = "0.0.0.0"

        parser = ArgumentParser(name)
        parser.add_argument("--service_name", default="REST service", type=str, help="Name of the web service")
        parser.add_argument("--service_info", default="REST service", type=str, help="Description of web service")
        parser.add_argument(
            "--port",
            default=default_port,
            type=int,
            help="port on which the HTTP REST listens, default: '" + str(default_port) + "'",
        )
        parser.add_argument(
            "--address",
            default=default_address,
            type=str,
            help="address on which the HTTP REST listens, default: '" + str(default_address) + "'",
        )
        parser.add_argument(
            "--threaded",
            help="If the server should be threaded. (Not recommended, run it as gunicorn instead)",
            default=False,
            type=bool,
        )
        parser.add_argument("--prefix", default="", type=str, help="Prefix to the endpoint path")
        parser.add_argument("--add_prefix_to_url", default=False, type=bool, help="Prefix to the endpoint path")
        return parser

    def __init__(self, args, app, name="RESTServer"):
        """
        Creates a REST server requiring a port specified in the passed args; the server listens on the port.
        :param args: a port specified in the passed args
        :return: None
        """
        self.logger = Logger(args, name=name)
        self.app = app
        self.port = args[name].port
        self.address = args[name].address
        self.threaded = args[name].threaded
        self.service_name = args[name].service_name
        self.service_info = args[name].service_info
        self.prefix = args[name].prefix
        self.add_prefix_to_url = args[name].add_prefix_to_url

        self.app.config["PROPAGATE_EXCEPTIONS"] = True
        self.healtcheck_init()
        self.trace_on_abort()

    def add_url_rule(self, method, endpoint="", view_func=None, methods=["POST"]):
        url = self.prefix + method if self.add_prefix_to_url else method
        self.app.add_url_rule(url, endpoint=endpoint, view_func=view_func, methods=methods)

    def healtcheck_view(self):
        """
        curl -X GET http://address:port/v2/ping
        """
        json_response = json.dumps(
            {
                ApiResponse.STATUS: {ApiResponse.STATUS_CODE: 200, ApiResponse.STATUS_TEXT: "OK"},
                "_service_info": {"_name": self.service_name, "_info": self.service_info},
            }
        )
        return Response(json_response, mimetype="application/json")

    def healtcheck_init(self):
        self.app.add_url_rule(
            python_api.Method.PING, endpoint="ping", view_func=self.healtcheck_view, methods=["GET", "POST", "OPTIONS"]
        )

    def trace_on_abort(self):
        signal.signal(signal.SIGABRT, self.print_trace)

    def print_trace(self, sig, frame):
        message = "SIGNAL ABORT: "
        message += "".join(traceback.format_stack(frame))
        try:
            raise Exception(message)
        except Exception:
            self.logger.sentry_exception()
            self.logger.error(message)

    def run(self):
        """
        Start simple server.
        Don't call this method, when we start this server with gunicorn,...
        :return: None
        """
        if self.threaded:
            self.app.run(host=self.address, port=self.port, threaded=True)
        else:
            self.app.run(host=self.address, port=self.port, threaded=False, processes=1)
