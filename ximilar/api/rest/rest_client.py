import json
import requests

from ximilar.api.processor import RequestProcessor
from ximilar.base.config import ArgumentParser
from ximilar.base.logger import Logger


class RESTClient(RequestProcessor):
    """
    A stub that just sends all requests to a specified server/method.
    """

    @staticmethod
    def get_arg_parser(name="RESTClient"):
        default_url = "http://localhost:4005/v2/descriptor"
        parser = ArgumentParser(name=name)
        parser.add_argument(
            "--server_url",
            default=default_url,
            help="http://host:port/method of a service to which this client connects, default: '" + default_url + "'",
        )
        parser.add_argument(
            "--headers", default={}, type=dict, help="headers to be added to the REST call (dictionary)"
        )
        return parser

    def __init__(self, args, condition=None, modified_fields=None, data_cleaner=None, name="RESTClient"):
        super().__init__(args, data_cleaner, name=name)
        self.logger = Logger(args, "RESTClient")
        self.logger.info("starting loggin RESTClient")

        self.url = args[name].server_url
        self.headers = args[name].headers
        self.condition = condition
        self.modified_fields = modified_fields
        self.MAX_RETRIES = 2
        self.TIMEOUT = 100

    def call_rest(self, json_request, headers):
        """
        Given a JSON record, this method actually connects to the REST service assuming that it expects a JSON and
        returns a modified JSON record
        :param headers: headers from the request
        :param json_request: JSON record as string
        :return:
        """
        # print(self.url)
        merge_headers = headers.copy() if headers is not None else {}
        for key, value in self.headers.items():
            merge_headers[key] = value
        self.logger.debug(
            "calling request "
            + self.url
            + " -d '"
            + json.dumps(json_request, ensure_ascii=False)[0:300]
            + "...(shortened)'"
        )
        response = requests.post(self.url, json=json_request, timeout=self.TIMEOUT, headers=merge_headers)
        self.logger.debug("  ...with result: " + str(response))
        # [requests.codes.ok, requests.codes.created, requests.codes.partial, requests.codes.accepted, ...
        if response.status_code < 200 or response.status_code >= 400:
            raise Exception(
                'error occurred when processing record "'
                + json.dumps(json_request)[0:200]
                + '...(shortened)"; response: '
                + str(response)
                + "\n"
            )
        return response.content

    def process_request(self, json_request, headers):
        response = None
        error = None
        for i in range(self.MAX_RETRIES):
            try:
                response = self.call_rest(json_request, headers)
                return self.clean_response(json.loads(response.decode("utf-8"))), headers
            except Exception as e:
                self.logger.info(
                    "processing exception: retry: " + str(i) + ": " + str(e) + "; response: " + str(response)
                )
                error = e
        else:
            self.logger.error(
                'error occurred when processing record: "'
                + json.dumps(json_request)[0:200]
                + '...(shortened)"; response: '
                + str(response)
                + "; error: "
                + str(error)
            )
            raise Exception(
                'error occurred when processing record "'
                + json.dumps(json_request)[0:200]
                + '...(shortened)": max retries ('
                + str(self.MAX_RETRIES)
                + ") exceeded!"
            )

    def pre_condition(self, json_request):
        return self.condition(json_request) if self.condition else True

    def pre_condition_desc(self):
        return "this condition is checked: " + self.condition if self.condition else "no condition known"

    def get_modified_fields(self):
        return self.modified_fields
