from ximilar.api.common_api import Record
import ximilar.api.common_api as common_api


class Request(common_api.Request):
    # Constants
    QUERY_REC = "query_record"
    FIELDS = "fields_to_return"
    APPROX = "cand_set_size"
    COUNT = "count"
    K = "k"
    RADIUS = "radius"
    FILTER = "filter"
    FROM = "from"
    PRODUCT_FIELD = "product_field"

    @staticmethod
    def random(count=20, fields_to_return=[Record.ID, Record.URL, Record.FILE], filter=None):
        """
        Creates a request JSON for random method
        :param count: How many images we need. Default 20.
        :return: dictionary/json request from messif.
        """
        ret_val = {Request.COUNT: count, Request.FIELDS: fields_to_return}
        if filter:
            ret_val[Request.FILTER] = filter
        return ret_val

    @staticmethod
    def filter(record, fields_to_return=None):
        if fields_to_return:
            return {Request.FILTER: record, Request.FIELDS: fields_to_return}
        return {Request.FILTER: record}

    @staticmethod
    def crud(objects, fields_to_return=None):
        if fields_to_return:
            return {Request.RECORDS: objects, Request.FIELDS: fields_to_return}
        return {Request.RECORDS: objects}

    @staticmethod
    def knn(
        query,
        k=10,
        fields_to_return=[Record.ID, Record.URL, Record.FILE],
        approximation=None,
        filter=None,
        additional_fields=None,
    ):
        ret_val = {Request.QUERY_REC: query, Request.K: k, Request.FIELDS: fields_to_return}
        if additional_fields:
            ret_val.update(additional_fields)
        if approximation:
            ret_val[Request.APPROX] = approximation
        if filter:
            ret_val[Request.FILTER] = filter
        return ret_val


class Response(common_api.Response):
    """
    Static strings and methods to work with the Search (MESSIF) response
    """

    DISTANCES = "answer_distances"
    ANS_RECORDS = "answer_records"
    # SKIPPED_RECORDS = "skipped_records"
    ANS_COUNT = "answer_count"

    @staticmethod
    def get_first_obj(response):
        if Response.ANS_RECORDS not in response or len(response[Response.ANS_RECORDS]) == 0:
            return None
        return response[Response.ANS_RECORDS][0]

    @staticmethod
    def get_answer_objects(response):
        if Response.ANS_RECORDS not in response:
            return None
        return response[Response.ANS_RECORDS]


class Method:
    INSERT = "/v2/insert"
    DELETE = "/v2/delete"
    GET = "/v2/get"
    UPDATE = "/v2/update"

    PING = "/v2/ping"
    RANDOM = "/v2/random"
    KNN_VISUAL = "/v2/visualKNN"
    KNN_VISUAL_TAGS = "/v2/visualTagsKNN"
    RANK_VISUAL = "/v2/rankRecordsVisual"
    RANK_VISUAL_TAGS = "/v2/rankRecordsVisualTags"
    GET_COUNT = "/v2/getRecordCount"
    NEAR_DUPS = "/v2/nearDuplicates"
    ALL_PAIRS = "/v2/allNearDupPairs"
    RANGE = "/v2/range"
    ANNOTATE = "/v2/annotate"
    ALL_RECORDS = "/v2/allRecords"


class MessifAPIClient(common_api.APIClient):
    """
    Class which encapsulates methods for working with messif server.
    """

    @staticmethod
    def get_arg_parser(name="SearchAPI"):
        return common_api.APIClient.get_arg_parser(name)

    def __init__(self, args, name="SearchAPI", server_url=None):
        common_api.APIClient.__init__(self, args, name, server_url=server_url)

    def random(
        self, count=20, fields_to_return=[Record.ID, Record.URL, Record.FILE], verbose=False, headers={}, filter=None
    ):
        """
        Calls post method on 'messif/v2/random' which returns random images id.
        :param count: How many images we need. Default 20.
        :return: dictionary/json response from messif.
        """
        return self.call(
            Method.RANDOM,
            Request.random(count, fields_to_return=fields_to_return, filter=filter),
            verbose,
            headers=headers,
        )

    def filter(self, filter_record, fields_to_return=None, verbose=False, headers={}):
        """
        Calls post method on /v2/allRecords and filter results.
        :param filter_record: criterion for search in db
        """
        request = Request.filter(filter_record, fields_to_return)
        return self.call(Method.ALL_RECORDS, request, verbose=verbose, headers=headers)

    def knn(
        self,
        image_rec,
        k=20,
        fields_to_return=[Record.ID, Record.URL, Record.FILE],
        approximation=None,
        filter=None,
        additional_fields=None,
        visual_and_tags=False,
        verbose=False,
        headers={},
    ):
        """
        Calls post method on 'messif/v2/visualKNN' which returns k nearest neighbours.
        :param visual_and_tags: if true, the visual+tags similarity is used instead of pure visual similarity
        :param approximation: the number of objects accessed during the approximate kNN evaluation by PPP-Codes index.
            If it is None, the index default is used (the parameter is not sent)
        :param image_rec: Id of image we want process for visual knn algorithm.
        :param k: How many images we need. Default 20.
        :return: dictionary/json response from messif.
        """
        request = Request.knn(
            image_rec,
            k,
            fields_to_return,
            approximation=approximation,
            filter=filter,
            additional_fields=additional_fields,
        )
        if visual_and_tags:
            return self.call(Method.KNN_VISUAL_TAGS, request, verbose, headers=headers)
        return self.call(Method.KNN_VISUAL, request, verbose, headers=headers)

    def rank_objects(self, image_rec, objects, k=20, visual_and_tags=False, verbose=False, headers={}):
        """
        Calls post method on 'messif/v2/rankObjectsVisual' which returns k nearest neighbours out of the passed objects.
        :param visual_and_tags: if true, the visual+tags similarity is used instead of pure visual similarity
        :param image_rec: Id of image we want process for visual knn algorithm.
        :param k: How many images we need. Default 20.
        :return: dictionary/json response from messif.
        """
        request = Request.knn(image_rec, k)
        request[Request.RECORDS] = objects
        if visual_and_tags:
            return self.call(Method.RANK_VISUAL_TAGS, request, verbose, headers=headers)
        return self.call(Method.RANK_VISUAL, request, verbose, headers=headers)

    def crud(self, objects, method=Method.INSERT, fields_to_return=None, verbose=False, headers={}):
        request = Request.crud(objects, fields_to_return)
        return self.call(method, request, verbose, headers=headers)
