"""
 This file contains constants and basic manipulation methods that are common for both MESSIF-like and python APIClient
"""

import json
import requests
import os
import re

from ximilar.base.config import ArgumentParser


class Record:
    """
    This class contains string constants used within a data record (JSON) that are common for both MESSIF and python APIClient
    """

    ID = "_id"
    URL = "_url"
    FILE = "_file"
    CACHED_FILE = "_cached_file"
    TYPES = "_types"
    BASE64 = "_base64"
    IMG_DATA = "_img_data"
    PIL_IMG_DATA = "_pil_img_data"
    FRAMES = "_frames"
    VISUAL_DESC = "_descriptor"
    REC_STATUS = "_status"
    IMG_HASH = "_img_hash"

    HASH_BMH0 = "_bmh0"
    HASH_BMH1 = "_bmh1"
    HASH_AHASH = "_ahash"
    HASH_CMHASH = "_cmhash"
    HASH_MHHASH = "_mhhash"
    HASH_PHASH = "_phash"
    HASH_RVARHASH = "_rvarhash"

    @staticmethod
    def get_rec_id_str(json_record):
        if Record.ID in json_record:
            return str(json_record[Record.ID])
        if Record.URL in json_record:
            return str(json_record[Record.URL])
        if Record.FILE in json_record:
            return str(json_record[Record.FILE])
        if Record.BASE64 in json_record:
            return json_record[Record.BASE64][:16]
        return "''"


class Request:
    """
    Fields used used in both APIs and basic methods to manipulate with a request.
    """

    RECORDS = "records"
    LANG = "lang"
    TAGGING_MODE = "tagging_mode"
    GENERATE_URL = "generate_url"

    @staticmethod
    def create_request(image_record, settings={}):
        if isinstance(image_record, dict):
            request = {Request.RECORDS: [image_record]}
            request = Request.insert_settings(request, settings=settings)
        else:
            request = Request.create_request_array(image_record, settings=settings)
        return request

    @staticmethod
    def create_request_array(image_records, settings={}):
        request = {Request.RECORDS: image_records}
        return Request.insert_settings(request, settings=settings)

    @staticmethod
    def insert_settings(request, settings):
        for key in list(settings.keys()):
            request[key] = settings[key]
        return request


class Response:
    """
    Class containing field strings used in the APIClient responses
    """

    STATUS = "status"
    STATUS_CODE = "code"
    STATUS_TEXT = "text"
    STATUS_ERROR = "error description"

    STATISTICS = "statistics"
    STATS_PROC_TIME = "processing time"
    STATS_PROC_ID = "proc_id"

    STATUS_CODE_OK = 200
    STATUS_CODE_MIXED_RESULT = 300
    STATUS_CODE_BAD_REQUEST = 400
    STATUS_CODE_UNAUTHORIZED = 401
    STATUS_CODE_FORBIDDEN = 403
    STATUS_CODE_NOT_FOUND = 404
    STATUS_CODE_ERROR = 500

    SKIPPED_RECORDS = "skipped_records"

    @staticmethod
    def is_successful(response):
        if Response.STATUS not in response:
            return False
        code = response[Response.STATUS][Response.STATUS_CODE]
        return 200 <= code < 300


class APIClient(object):
    """
    This class should be instantiated and it provides basic methods for calling Ximilar APIClient (both Search and ML).
    """

    @staticmethod
    def get_arg_parser(name="APIClient"):
        parser = ArgumentParser(name=name)
        parser.add_argument(
            "--server",
            default=os.environ.get(APIClient.convert(name).upper() + "_SERVER", "http://localhost:30002"),
            help="URL of the server, default: http://localhost:30002",
        )
        return parser

    @staticmethod
    def convert(name):
        """ Converts CamelCase to snake_case """
        s1 = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
        return re.sub("([a-z0-9])([A-Z])", r"\1_\2", s1).lower()

    def __init__(self, args, name="APIClient", server_url=None):
        from ximilar.base.logger import Logger

        self.logger = Logger(args, name=name)

        if server_url is not None:
            self.server_url = server_url
        else:
            self.server_url = os.environ.get(APIClient.convert(name).upper() + "_SERVER", args[name].server)

        if not self.server_url:
            raise ValueError(
                "the MESSIF server URL must be specified either by line arguments or by 'server_url' parameter"
            )

    def call(self, method, request, verbose=True, headers={}):
        """
        Calls given APIClient method sending there given JSON request.
        :param method: APIClient method to be called on given server
        :param request: a JSON request to be sent to the method by POST
        :param verbose: if true, both request and answer are printed out to stdout
        :return: the JSON-parsed response; exception is raised if the response is not JSON
        """
        header_string = " ".join(['-H "' + key + ": " + value + '"' for key, value in headers.items()])
        self.logger.debug("CALLING " + self.server_url + method + " " + header_string)
        self.logger.debug("With request -d '" + json.dumps(request) + "'")

        result = None
        try:
            result = requests.post(self.server_url + method, json=request, headers=headers, timeout=10)
            json_result = result.json()
            self.logger.debug("RESPONSE: " + json.dumps(json_result))
            return json_result
        except requests.RequestException as e:
            self.logger.sentry_exception()
            self.logger.error("ERROR calling " + self.server_url + method)
            raise Exception("Error requests exception!" + str(e))
        except Exception as e:
            self.logger.sentry_exception()
            self.logger.error("ERROR calling " + self.server_url + method)

            if result is not None:
                self.logger.debug("WRONG RESPONSE: " + str(e))
            raise Exception("Wrong response from CALL method!")
