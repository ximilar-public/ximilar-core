from ximilar.api import common_api
from ximilar.api.common_api import Request

CORRECT_FIELD = "correct"
PREDICTED_FIELD = "predicted"
TYPES = "types"


class Record(common_api.Record):
    WORDS = "tags"

    # settings of json records
    SETTINGS = "_settings"
    LANG = "lang"
    TAGGING_MODE = "tagging_mode"
    MODE = "mode"
    TASK_ID = "task_id"
    VERSION = "version"
    TIME_STATS = "time_stats"

    # neural base
    INPUT = "_input"
    TARGET = "_target"
    DROPOUT = "_dropout"
    LEARN_RATE = "_learn_rate"
    OUTPUT_NAME = "_output_name"
    OUTPUT_NAMES = "_output_names"
    BATCH_SIZE = "_batch_size"
    LENGTH_OF_SENTENCES = "_length_of_sentences"

    # image
    WIDTH = "_width"
    HEIGHT = "_height"
    CHANNELS = "_channels"
    DOMINANT_COLORS = "_dominant_colors"
    COLOR_LAYOUT = "_color_layout"
    SHARPNESS = "_sharpness"
    LUVCOLORS = "luv_colors"
    RGBCOLORS = "rgb_colors"
    COLORNAMES = "color_names"
    RGBHEXCOLORS = "rgb_hex_colors"
    PERCENTAGES = "percentages"
    BRISK = "_brisk"
    COLOR_SPACE = "_color_space"

    # video
    VIDEO_DATA = "_video_data"
    VIDEO_FRAMES = "_frame_ids"
    VIDEO_LENGTH_SECONDS = "_video_lenght_seconds"
    VIDEO_FRAME_ID = "_frame_id"
    VIDEO_FRAME_RATE = "_frame_rate"
    VIDEO_FRAME_COUNT = "_frame_count"
    VIDEO_SCENE_NUMBER = "_scene_number"
    VIDEO_SCENE_CHANGES = "_scene_change"
    VIDEO_SCENE_CHANGES_IDS = "_frame_ids"
    VIDEO_SCENE_CHANGES_SECS = "_frame_secs"

    # Output from neural base
    FACES = "_faces"
    NUMBER_OF_PEOPLE = "_number_of_people"
    FRONT_VIEW = "_front_view"
    UNIQUE_FACES = "_unique_faces"
    BOUNDING_BOXES = "_bound_boxes"
    LANDMARKS = "_landmarks"
    CLASSES = "_categories"

    # Classification
    LABELS = "labels"
    TAGS = "_tags"
    TAGS_SIMPLE = "_tags_simple"
    BEST_LABEL = "best_label"
    PROBABILITY = "prob"
    NAME = "name"
    # a list of alternative names in case, we are not sure
    ALTERNATIVES = "alternative_names"

    # Object detection
    OBJECTS = "_objects"
    # xmin, ymin, xmax, ymax
    BOUNDING_BOX = "bound_box"
    OBJ_TYPE = "_obj_type"

    # Product recognition
    # collection (often 'customer identification')
    COLLECTION = "collection"
    # product_id
    PRODUCT_ID = "product_id"
    # search parameters to be passed to the search index
    SEARCH_PARAMS = "_search_params"

    # customer-specified category of the product
    CATEGORY = "category"
    CATEGORIES = "categories"

    # structure of neural base
    CLASS_NAMES = "_class_names"
    TAGS_TO_GENERATE = "_tags_to_generate"
    TOP_LEVEL = "top"
    MODEL_ID = "_model_id"
    NUMBER_CLASSES = "_number_of_classes"
    PATH = "_path"

    # pca
    PCA_TRANSFORM = "_pca_transform"

    # workers
    DEFAULT = "default"
    UUID = "_uuid"
    PROCESSED = "_processed"
    DTYPE = "_dtype"
    DSHAPE = "_dshape"


class Method:
    DESC_EXTRACTION = "/v2/descriptor"
    DESC_EXTRACTION_GRAY = "/v2/descriptor_gray"
    FEEDBACK = "/v2/feedback"
    TEXT2VEC = "/v2/text2vec"
    FACE_DETECT_PHOTO = "/v2/facedetect/photo"
    FACE_DETECT_VIDEO = "/v2/facedetect/video"
    OBJECT_DETECT_PHOTO = "/v2/objectdetect/photo"
    CLASSIFICATION = "/v2/classify"
    DETECT = "/v2/detect"
    TAGS = "/v2/tags"
    COLORS_TAGS = "/v2/colors_tags"
    DOMINANT_COLOR = "/v2/dominantcolor"
    PING = "/v2/ping"
    CUSTOM = "/v2/custom"
    DESC_TAGS = "/v2/desc_tags"
    DESC_BLUR_TAGS = "/v2/desc_blur_tags"

    PROD_INSERT = "/v2/product/insert"
    PROD_INSERT_CHECK = "/v2/product/check_insert"
    PROD_SEARCH = "/v2/product/search"
    PROD_DETECT = "/v2/product/detect"
    PROD_OBJ_SEARCH = "/v2/product/search_by_object"
    ALL_IDS = "/v2/allIDs"

    MERGE_DUPLICATES = "/v2/merge_duplicates"
    VISUAL_HASH = "/v2/visual_hash"


class PythonAPIClient(common_api.APIClient):
    """
    Class which encapsulates methods for working with a python server.
    """

    @staticmethod
    def get_arg_parser(name="PythonAPIClient"):
        return common_api.APIClient.get_arg_parser(name)

    def __init__(self, args, name="PythonAPIClient"):
        common_api.APIClient.__init__(self, args, name)

    def desc_extraction(self, image_rec, verbose=False, settings={}, headers={}):
        """
        Calls method that extracts VGG descriptor by calling method of given server
        :return: dictionary/json response.
        """
        return self.call(
            Method.DESC_EXTRACTION, Request.create_request(image_rec, settings=settings), verbose, headers=headers
        )

    def face_detect_photo(self, image_rec, verbose=False, headers={}):
        """
        Calls method that calls the text2vec on a single image record (with "keywords")
        :return: dictionary/json response.
        """
        return self.call(Method.FACE_DETECT_PHOTO, Request.create_request(image_rec), verbose, headers=headers)

    def classification(self, image_rec, verbose=True, headers={}):
        """
        Calls method that calls the text2vec on a single image record (with "keywords")
        :return: dictionary/json response.
        """
        return self.call(Method.CLASSIFICATION, Request.create_request(image_rec), verbose, headers=headers)

    def tags(self, image_rec, verbose=True, settings={}, headers={}):
        """
        Calls method that calls the text2vec on a single image record (with "keywords")
        :return: dictionary/json response.
        """
        return self.call(Method.TAGS, Request.create_request(image_rec, settings=settings), verbose, headers=headers)

    def dominant_color(self, image_rec, verbose=False, headers={}):
        """
        Calls method that calls the dominant color on a single image record (with "keywords")
        :return: dictionary/json response.
        """
        return self.call(Method.DOMINANT_COLOR, Request.create_request(image_rec), verbose, headers=headers)

    def custom(self, image_rec, method, verbose=False, settings={}, headers={}):
        """
        Calls method that calls the dominant color on a single image record (with "keywords")
        :return: dictionary/json response.
        """
        return self.call(method, Request.create_request(image_rec, settings=settings), verbose, headers=headers)

    def desc_tags(self, image_rec, verbose=False, settings={}, headers={}):
        """
        Calls method that calls the dominant color on a single image record (with "keywords")
        :return: dictionary/json response.
        """
        return self.call(
            Method.DESC_TAGS, Request.create_request(image_rec, settings=settings), verbose, headers=headers
        )
