import abc
import time

from ximilar.api.python_api import Record
from ximilar.api.common_api import Request
from ximilar.api.messif_api import Response
from ximilar.base.record_checker import RecordChecker
from ximilar.base.logger import Logger


class RequestProcessor(RecordChecker):
    """
    This is a base class for JSON request processors.
    """

    def __init__(self, args, data_cleaner=None, name="RequestProcessor"):
        """
        Constructor that gets a data cleaner to be applied after
        :param data_cleaner: 'records' cleaner to be used in 'clean_request' method
        :type data_cleaner: ximilar.base.data_cleaner.DataCleaner
        """
        self.name = name
        self.logger = Logger(args, name=name)
        self.data_cleaner = data_cleaner

    @abc.abstractmethod
    def process_request(self, json_request, headers):
        """
        Given a JSON record, this is the main method that should modify the given record.
        :param json_request: JSON object (map) as returned by JSON.parse
        :param headers: dict of HTTP headers from the request
        :return: the modified record (request) AND headers (a pair)
        :raise Exception if the processing fails
        """
        raise NotImplementedError

    @abc.abstractmethod
    def pre_condition(self, json_request):
        raise NotImplementedError

    @abc.abstractmethod
    def pre_condition_desc(self):
        raise NotImplementedError

    def pre_condition_exception(self):
        return Exception(self.pre_condition_desc())

    def clean_response(self, response):
        """
        This method takes a request a cleans all data that should not be returned.
        The fields of the request ARE MODIFIED.
        :param response: json response to be cleaned before returning
        :return: modified response (for convenience)
        """
        if not self.data_cleaner:
            return response
        if Response.ANS_RECORDS in response:
            response[Response.ANS_RECORDS] = self.data_cleaner.process_records(response[Response.ANS_RECORDS])
        if Response.SKIPPED_RECORDS in response:
            response[Response.SKIPPED_RECORDS] = self.data_cleaner.process_records(response[Response.SKIPPED_RECORDS])
        if Request.RECORDS in response:
            response[Request.RECORDS] = self.data_cleaner.process_records(response[Request.RECORDS])
        return response

    def compute_time_stats(self, request, start_time):
        """
        Computes time statistic for this RequestProcessor.
        :param request: json request
        :param start_time: start time of the processing
        :return: modified json request with time statistics
        """
        if not self.logger.log_debug:
            return request

        if Response.STATISTICS not in request:
            request[Response.STATISTICS] = {}

        if Record.TIME_STATS not in request[Response.STATISTICS]:
            request[Response.STATISTICS][Record.TIME_STATS] = {}

        request[Response.STATISTICS][Record.TIME_STATS][self.name] = time.time() - start_time
        return request
