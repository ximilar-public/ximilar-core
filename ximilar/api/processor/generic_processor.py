import time

from ximilar.api import messif_api
from ximilar.api.common_api import Request, Response
from ximilar.api.python_api import Record
from ximilar.api.processor.request_processor import RequestProcessor
from ximilar.base.config import ArgumentParser
from ximilar.base.exception import DeepException, ERRCODE, ERRMSG
from ximilar.base.data_processor import DataProcessor
from ximilar.base.data_cleaner import DataCleaner


class GenericProcessor(RequestProcessor):
    """
    This is a class that takes a JSON request and it applies one or more DataProcessors on the encapsulated data records.
    """

    @staticmethod
    def get_arg_parser(name="GenericProcessor"):
        parser = ArgumentParser(name)
        parser.add_argument(
            "--max_batch",
            help="The max size of batch(number of records) with which server can work",
            type=int,
            default=2500,
        )
        parser.add_argument(
            "--settings",
            help="Default settings in json records which we want to copy to every record",
            type=list,
            default=[Record.LANG, Record.COLORNAMES, Record.TAGGING_MODE, Record.MODE],
        )
        parser.add_argument(
            "--header_settings",
            help="Fields to copy from the headers to each record's _settings",
            type=list,
            default=[],
        )
        parser.add_argument(
            "--skipped_records",
            help="put 'bad records' to + '" + messif_api.Response.SKIPPED_RECORDS + "' (default False)",
            type=bool,
            default=False,
        )
        return parser

    def __str__(self):
        return "GenericProcessor"

    def __init__(self, args, data_processors, data_cleaner=None, name="GenericProcessor"):
        """
        Check if the given processors are of type DataProcessor and store them.
        :param data_processors: iterable with objects of type DataProcessor
        :return: None
        """
        super().__init__(args, data_cleaner, name=name)

        self.args = args[name]
        for processor in data_processors:
            if not isinstance(processor, DataProcessor):
                raise DeepException(500, "GenericProcessor:" + ERRMSG[ERRCODE.NOTINSTANCE])

        self.processors = data_processors
        self.processors_count = str(len(self.processors))

    def process_request(self, json_request, headers):
        """
        Process JSON record by applying processor(data loader, ...) on json.
        For example data loader processor is called for loading data.
        :param json_request: Represents json data of request from client.
        :param headers: map of HTTP headers from the request (can be None)
        :return: json data which will be further processed by another processor
                 or sent as response from server.
        :rtype: dict
        """
        start_time = time.time()

        json_request = self.extend_settings(json_request, headers)
        json_request = self.extend_status(json_request)

        json_request[Request.RECORDS], bad_records = self.process_batch(json_request[Request.RECORDS])

        if len(bad_records) > 0:
            if self.args.skipped_records:
                if Response.SKIPPED_RECORDS not in json_request:
                    json_request[Response.SKIPPED_RECORDS] = []
                json_request[Response.SKIPPED_RECORDS].extend(bad_records)
            else:
                json_request[Request.RECORDS].extend(bad_records)

        json_request = self.compute_time_stats(json_request, start_time)
        return self.clean_response(json_request), headers

    def extend_settings(self, json_request, headers):
        """
        Adds a "_settings" object into each record of the request; the settings contain all parameters (except "records")
        :param json_request: a parsed JSON-like API request
        :param headers: request headers
        :return: modified request
        """
        self.logger.debug("Extending each record by settings")

        for i in range(len(json_request[Request.RECORDS])):
            if Record.SETTINGS not in json_request[Request.RECORDS][i]:
                json_request[Request.RECORDS][i][Record.SETTINGS] = {}

            for setting in self.args.settings:
                # copy all params to _settings
                if setting in json_request:
                    json_request[Request.RECORDS][i][Record.SETTINGS][setting] = json_request[setting]
            for setting in self.args.header_settings:
                # copy given headers to _settings
                if setting in headers:
                    json_request[Request.RECORDS][i][Record.SETTINGS][setting] = headers[setting]

        self.logger.debug("Extending settings complete")
        return json_request

    def extend_status(self, json_request):
        """
        Adds a "status" object to each record of the request
        :param json_request: an API request that is supposed to contain "records"
        :return:
        """
        self.logger.debug("Extending each record by status information")

        for i in range(len(json_request[Request.RECORDS])):
            json_request[Request.RECORDS][i][Record.REC_STATUS] = {
                Response.STATUS_CODE: 200,
                Response.STATUS_TEXT: "OK",
            }
        return json_request

    def process_batch(self, data_records):
        """
        Given a json record with batch of samples, this method calls the processing.
        :param data_records:  json record
        :return: a pair: (list of JSON records, list of 'bad' JSON records that were not processed by all processors)
        :rtype: tuple
        """
        try:
            if len(data_records) > self.args.max_batch:
                raise DeepException(
                    code=Response.STATUS_CODE_FORBIDDEN,
                    msg="Number of records in batch exceeded number "
                    + str(len(data_records))
                    + "/"
                    + str(self.args.max_batch),
                )

            bad_records = []
            for index, processor in enumerate(self.processors):
                index = str(index + 1)

                self.logger.debug(
                    "%s/%s 1/4 Processing records with %s" % (index, self.processors_count, str(processor))
                )
                new_data_records = []

                for record in data_records:
                    # Checking precondition and if it fails remove the record from processing
                    if processor.pre_condition(record):
                        self.logger.debug(
                            "%s/%s 2/4 Precondition of %s records passed: %s"
                            % (index, self.processors_count, str(processor), str(processor.pre_condition_desc()))
                        )
                        new_data_records.append(record)
                    else:
                        self.logger.debug(
                            "%s/%s 2/4 Precondition of %s records failed: %s"
                            % (index, self.processors_count, str(processor), processor.pre_condition_desc())
                        )
                        # Inserting error status to record
                        if record[Record.REC_STATUS][Response.STATUS_CODE] == 200:
                            record[Record.REC_STATUS][Response.STATUS_CODE] = 500
                            record[Record.REC_STATUS][Response.STATUS_TEXT] = processor.pre_condition_desc()
                        bad_records.append(record)

                if len(new_data_records) > 0:
                    self.logger.debug(
                        "%s/%s 3/4 Processing %s records with %s"
                        % (index, self.processors_count, str(len(new_data_records)), str(processor))
                    )
                    new_data_records = processor.process_records(new_data_records)

                data_records = new_data_records
                self.logger.debug("%s/%s 4/4 Processing complete %s" % (index, self.processors_count, str(processor)))

                # If we are cleaning our data we need to clean also internal info from bad records
                if isinstance(processor, DataCleaner) and len(bad_records) > 0:
                    self.logger.debug("Cleaning records that were not fully processed with %s" % (str(processor)))
                    bad_records = processor.process_records(bad_records)

            return data_records, bad_records
        except DeepException as e:
            self.logger.sentry_exception()
            raise DeepException(e.code, e.msg)
        except Exception as e:
            self.logger.debug("Exception thrown during processing: " + str(e))
            self.logger.sentry_exception()
            raise Exception(str(e))

    def pre_condition(self, json_request):
        return Request.RECORDS in json_request or len(self.processors) == 0

    def pre_condition_desc(self):
        return ERRMSG[ERRCODE.CONTRECORD] % Request.RECORDS

    def get_modified_fields(self):
        return [Request.RECORDS]
