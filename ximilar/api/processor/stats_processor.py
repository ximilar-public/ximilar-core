import time
import uuid

from ximilar.api.common_api import Response, Request
from ximilar.api.python_api import Record
from ximilar.api.processor.request_processor import RequestProcessor
from ximilar.base.exception import DeepException, ERRCODE, ERRMSG
from ximilar.base.data_processor import DataProcessor


@DeprecationWarning
class StatsProcessor(RequestProcessor):
    """
    This processor encapsulates another request processor and adds statistics to the request answer (like processing
    time).
    """

    def __str__(self):
        return "StatsProcessor"

    def __init__(self, request_processor, data_cleaner=None):
        """
        Get the encapsulated request processor
        :param request_processor: encapsulated request processor
        :return: None
        """
        super().__init__(data_cleaner)
        self.id = str(uuid.uuid4())

        if not isinstance(request_processor, RequestProcessor):
            raise Exception(str(request_processor) + " in StatsProcessor is not RequestProcessor instance.")

        self.request_processor = request_processor

    def process_request(self, json_request, headers):
        """
        Process a JSON dictionary request and
        :param json_request: Represents json data of request from client.
        :param headers: map of HTTP headers from the request (can be None)
        :return: json data which will be further processed by another processor
                 or sent as response from server.
        :rtype: dict
        """
        start_time = time.time()
        result, headers = self.request_processor.process_request(json_request, headers)
        result[Response.STATISTICS] = {
            Response.STATS_PROC_TIME: time.time() - start_time,
            Response.STATS_PROC_ID: self.id,
        }

        # if we have statistic from each processor then store it into statistics
        if (
            Request.RECORDS in result
            and len(result[Request.RECORDS]) > 0
            and Record.TIME_STATS in result[Request.RECORDS][0]
        ):
            result[Response.STATISTICS][Record.TIME_STATS] = result[Request.RECORDS][0][Record.TIME_STATS]

            # safely delete it from each record
            for i in range(len(result[Request.RECORDS])):
                if Record.TIME_STATS in result[Request.RECORDS][i]:
                    del result[Request.RECORDS][i][Record.TIME_STATS]

        return self.clean_response(result), headers

    def pre_condition(self, json_request):
        return self.request_processor.pre_condition(json_request)

    def pre_condition_desc(self):
        return self.request_processor.pre_condition_desc()

    def get_modified_fields(self):
        return self.request_processor.get_modified_fields + [Response.STATISTICS]
