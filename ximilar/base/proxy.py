class ReverseProxied(object):
    """ Wrap the application in this middleware and configure the
        front-end server to add these headers, to let you quietly bind
        this to a URL other than / and to an HTTP scheme that is
        different than what is used locally.

        In nginx:

            location /myprefix {
                proxy_pass http://192.168.0.1:5001;
                proxy_set_header Host $host;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Prefix /myprefix;
            }

        :param app: the WSGI application

        :param path_prefix: URL prefix of the application
    """

    def __init__(self, app, path_prefix=None):
        self.app = app
        self.path_prefix = path_prefix

    def __call__(self, environ, start_response):
        prefix = environ.get("HTTP_X_FORWARDED_PREFIX") or self.path_prefix or ""
        prefix = prefix.rstrip("/")
        if prefix:
            environ["SCRIPT_NAME"] = prefix
            path_info = environ["PATH_INFO"]
            if path_info.startswith(prefix):
                environ["PATH_INFO"] = path_info[len(prefix) :]

        scheme = environ.get("HTTP_X_FORWARDED_PROTO", "")
        if scheme:
            environ["wsgi.url_scheme"] = scheme
        return self.app(environ, start_response)

    def __getattr__(self, attr):
        """ Proxy attributes we don't know to the wrapped app.
        """
        return getattr(self.app, attr)
