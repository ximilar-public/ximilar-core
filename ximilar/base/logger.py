import logging
import logging.handlers
import sys
import os

from raven import Client
from concurrent_log_handler import ConcurrentRotatingFileHandler

from ximilar.api.python_api import Request
from ximilar.api.python_api import Record
from ximilar.base.config import ArgumentParser


class LogFilter(object):
    def __init__(self, level):
        self.__level = level

    def filter(self, log_record):
        return log_record.levelno <= self.__level


class Logger(object):
    """
    Our Logging class which encapsulates logging module from python library.
    It can also use the sentry error tracker if application has sentry turned on with param.
    This class use our argparse module for parsing input arguments.
    """

    @staticmethod
    def get_arg_parser(name="Logger"):
        parser = ArgumentParser(name=name)
        parser.add_argument("--no_log", help="If we do not want logging", default=False, type=bool)
        parser.add_argument("--log_id", help="Identification of root logger(default=pid)", default=str(os.getpid()))
        parser.add_argument(
            "--log_dir", help="Path for log of some subsystem included prefix of filename", default="logs"
        )
        parser.add_argument(
            "--log_levels",
            default=True,
            type=bool,
            help="True (default) if every level(debug, info, error) should be together in one file!",
        )
        parser.add_argument(
            "--log_debug", default=False, type=bool, help="False (default) if we do not want debug informations!"
        )
        parser.add_argument(
            "--stdout", default=False, type=bool, help="False (default) if we do not want to show logs to stdout"
        )
        parser.add_argument("--sentry_dsn", default="", help="url address to sentry project")
        return parser

    def __init__(self, args, name="Logger"):
        self.name = name

        # Logger args are global across entire application so we constantly set name to "Logger"
        self.no_log = args["Logger"].no_log
        self.stdout = args["Logger"].stdout
        self.log_id = args["Logger"].log_id
        self.log_levels = args["Logger"].log_levels
        self.log_dir = args["Logger"].log_dir
        self.log_debug = bool(os.environ.get("DEBUG", args["Logger"].log_debug))
        self.sentry_dsn = args["Logger"].sentry_dsn

        # create directory
        self.path_prefix = self.log_dir + "/" + self.log_id
        self.logger = logging.getLogger(self.log_id)
        self.create_dir(self.log_dir)

        # If we want to run logging with sentry error tracker
        if self.sentry_dsn:
            self.sentry_client = Client(self.sentry_dsn)

        if self.no_log:
            return

        if not len(self.logger.handlers):
            if self.log_levels:
                handler = self.create_handler(self.path_prefix + ".log")
                if self.log_debug:
                    handler.setLevel(logging.DEBUG)
                else:
                    handler.setLevel(logging.INFO)

                self.logger.addHandler(handler)
            else:
                handler1 = self.create_handler(self.path_prefix + ".info.log")
                handler1.setLevel(logging.INFO)
                handler1.addFilter(LogFilter(logging.INFO))

                self.logger.addHandler(handler1)
                if self.log_debug:
                    handler2 = self.create_handler(self.path_prefix + ".debug.log")
                    handler1.setLevel(logging.DEBUG)
                    self.logger.addHandler(handler2)
                handler3 = self.create_handler(self.path_prefix + ".error.log")
                handler3.setLevel(logging.ERROR)
                self.logger.addHandler(handler3)

            if self.log_debug:
                self.logger.setLevel(logging.DEBUG)
            else:
                self.logger.setLevel(logging.INFO)

        self.logger.info("Starting Logging in " + self.name + ", Process ID: " + str(os.getpid()))

    def create_dir(self, path):
        if not os.path.exists(path):
            os.mkdir(path)

    def create_handler(self, path):
        """ Create rotating file handler in specific path.
        :param path: destination of the file handler
        :return: file handler object
        """
        handler = ConcurrentRotatingFileHandler(path, mode="a", maxBytes=10000000, backupCount=5, encoding="utf-8")
        handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(message)s\n"))
        return handler

    def sentry_msg(self, msg):
        """
        If application is using sentry error tracker then use it to send specific message.
        :param: msg to send
        :return: None
        """
        if self.sentry_dsn:
            try:
                self.sentry_client.captureMessage(msg)
            except:
                pass

    def sentry_exception(self):
        """
        If application is using sentry error tracker then use it to track handled exception.
        :return: None
        """
        if self.sentry_dsn:
            try:
                self.sentry_client.captureException()
            except Exception as e:
                pass

    def print_to_stdout(self, msg, stdout=False):
        """
        If we want to print the logging messages to stdout also. Default FALSE.
        :param msg: string to print
        :return: None
        """
        if stdout or self.stdout:
            try:
                print(msg)
            except:
                print("Exception when printing to stdout! Possible unicode error.")

    def info(self, msg, stdout=False):
        """
        Logging the info message
        :param msg: string message
        :param stdout: False(default). True if we want to print the message to the stdout.
        """
        if self.no_log:
            return

        try:
            self.print_to_stdout(msg, stdout)
            self.logger.info(self.name + ": " + msg)
            sys.stdout.flush()
        except Exception as e:
            self.sentry_exception()

    def debug(self, msg, stdout=False):
        """
        Logging the debug message
        :param msg: string message
        """
        if self.no_log:
            return

        try:
            if self.log_debug:
                self.print_to_stdout(msg, stdout)
            self.logger.debug(self.name + ": " + msg)
            sys.stdout.flush()
        except Exception as e:
            self.sentry_exception()

    def error(self, msg, stdout=False):
        """
        Logging the error message
        :param msg: string message
        :param stdout: False(default). True if we want to print the message to the stdout.
        """
        if self.no_log:
            return

        try:
            self.print_to_stdout(msg, stdout)
            self.logger.error(self.name + ": " + msg)
            sys.stdout.flush()
        except Exception as e:
            self.sentry_exception()

    def clean_json(self, json_data):
        """
        Remove _base64, _img_data from json.
        :param json_data:
        :return:
        """
        if Request.RECORDS in json_data:
            for i in range(len(json_data[Request.RECORDS])):
                self._clean_from_record(json_data[Request.RECORDS][i])
        else:
            json_data = self._clean_from_record(json_data)
        return json_data

    def _clean_from_record(self, record):
        to_delete = [Record.IMG_DATA, Record.BASE64]

        for field in to_delete:
            if field in record:
                self.debug("Removed field " + field + " for debug from json")
                del record[field]
        return record
