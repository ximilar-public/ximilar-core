from abc import ABCMeta


class Model(object, metaclass=ABCMeta):
    """
    Abstract Class of Model.
    """

    def build_model(self, trainable):
        raise NotImplementedError

    def save_model(self, path=None):
        raise NotImplementedError

    def load_model(self, path=None):
        raise NotImplementedError

    def get_output(self, data):
        raise NotImplementedError

    def __str__(self):
        return "Abstract Model"
